/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"fmt"
	"runtime"
	"strconv"
	"time"

	"github.com/briandowns/spinner"
	"github.com/packetstracer/celegram/common"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	syncAlerts *bool
	ping       *bool
)

var monitorCmd = &cobra.Command{
	Use:   "monitor",
	Short: "Monitor your node health",
	Long: `Monitor launches a cron task that monitors the health of an node node, and 
	send alerts to Telegram and Discord.`,
	Run: func(cmd *cobra.Command, args []string) {
		common.PrintTitle("🪐 Bridge Monitor started ", "h1")
		fmt.Println("")

		initMonitor()

		// Keep executing main() until all go subroutines end
		runtime.Goexit()
		fmt.Println("Exiting program")
	},
}

func initMonitor() {
	monitorQuitChannel := make(chan bool)

	ctx := context.Background()
	url := common.GetNodeUrl()
	token := common.GetNodeAuthToken()

	if *syncAlerts {
		common.SendStartAlert(ctx, url, token)
	}

	if *ping {
		SchedulePingCronTask(ctx, url, token, monitorQuitChannel)
	}

	ScheduleCatchupCronTask(ctx, url, token, monitorQuitChannel)
}

func manageCatchup(ctx context.Context, url string, token string, s *spinner.Spinner, errorCounter uint8, retries uint8) error {
	metrics, err := common.FetchMetrics(ctx, url, token)

	if err != nil || metrics.ID == "" {
		errorCounter = errorCounter + 1

		if errorCounter >= retries {
			err := &common.CustomError{
				Msg: "Metrics error (" + strconv.FormatUint(uint64(errorCounter), 10) + ")",
			}

			s.Suffix = common.PrintCatchupStatus(metrics, err)
		}

		return err
	} else if errorCounter > 0 {
		errorCounter = 0
	}

	s.Suffix = common.PrintCatchupStatus(metrics, err)

	if *syncAlerts {
		common.ManageBridgeAlerts(metrics)
	}

	return err
}

func ScheduleCatchupCronTask(ctx context.Context, url string, token string, quit chan bool) {
	go func() {
		retries := viper.GetInt("http_request_retries")
		interval := viper.GetUint64("scan_interval_in_secs")

		s := spinner.New(spinner.CharSets[14], 100*time.Millisecond)
		s.Color("magenta", "bold")
		s.Suffix = " ⏳ Init...\n"
		s.Start()

		var errorCounter uint8 = 0
		manageCatchup(ctx, url, token, s, errorCounter, uint8(retries))

		for range time.Tick(time.Duration(interval) * time.Second) {
			select {
			case <-quit:
				return

			default:
				err := manageCatchup(ctx, url, token, s, errorCounter, uint8(retries))

				// TODO: I think this is not needed, check and remove if not needed
				if err != nil {
					continue
				}
			}
		}

		s.Stop()
	}()
}

func SchedulePingCronTask(ctx context.Context, url string, token string, quit chan bool) {
	go func() {
		if !common.HasCommunicationChannelsEnabled() {
			common.PrintError("🆘 No communication channels configured! Update your ~/.celegram config file.")
			return
		}

		retries := viper.GetInt("http_request_retries")
		retry_interval := viper.GetInt("http_request_retry_interval_in_secs")
		interval := viper.GetUint64("ping_interval_in_secs")

		metrics, err := common.FetchMetrics(ctx, url, token)
		if err != nil || metrics.ID == "" {
			common.PrintError("🆘 Metrics error: " + err.Error())
		}

		common.SendPingMsg(metrics)

		for range time.Tick(time.Duration(interval) * time.Second) {
			select {
			case <-quit:
				return

			default:
				for i := 0; i < retries; i++ {
					metrics, err := common.FetchMetrics(ctx, url, token)
					if err != nil {
						common.PrintError("🆘 Metrics error: " + err.Error())
					}

					err = common.SendPingMsg(metrics)
					if err == nil {
						break
					}

					if i == retries-1 {
						// TODO : print error Ping not Sent!
						fmt.Println("Ping not sent : " + err.Error())
					} else {
						time.Sleep(time.Duration(retry_interval) * time.Second)
					}
				}
			}
		}
	}()
}

func init() {
	rootCmd.AddCommand(monitorCmd)

	syncAlerts = monitorCmd.Flags().BoolP("sync", "s", false, "Send bridge sync alerts")
	ping = monitorCmd.Flags().BoolP("ping", "p", false, "Send ping message each interval")
}
