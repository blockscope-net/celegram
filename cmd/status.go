/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"

	"github.com/packetstracer/celegram/common"
	"github.com/spf13/cobra"
)

var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "Get bridge status data",
	Long:  `Get the status information about the bridge.`,
	Run: func(cmd *cobra.Command, args []string) {
		common.PrintTitle("🪐 Bridge Status started ", "h1")

		ctx := context.Background()
		url := common.GetNodeUrl()
		authToken := common.GetNodeAuthToken()

		nodeInfo, err := common.GetNodeInfo(ctx, url, authToken)
		if err != nil {
			common.PrintError("🆘 Could not get GetNodeInfo")
		}

		ready, err := common.IsNodeReady(ctx, url, authToken)
		if err != nil {
			common.PrintError("🆘 Could not get IsNodeReady")
		}

		balance, err := common.GetStateBalance(ctx, url, authToken)
		if err != nil {
			common.PrintError("🆘 Could not get GetStateBalance")
		}

		localHead, err := common.GetHeaderLocalHead(ctx, url, authToken)
		if err != nil {
			common.PrintError("🆘 Could not get GetHeaderLocalHead")
		}

		networkHead, err := common.GetHeaderNetworkHead(ctx, url, authToken)
		if err != nil {
			common.PrintError("🆘 Could not get GetHeaderNetworkHead")
		}

		p2pInfo, err := common.GetP2pInfo(ctx, url, authToken)
		if err != nil {
			common.PrintError("🆘 Could not get GetP2pInfo")
		}

		bandwidthStats, err := common.GetP2pBandwidthStats(ctx, url, authToken)
		if err != nil {
			common.PrintError("🆘 Could not get GetBandwidthStats")
		}

		// TODO : HERE add GetP2pPeerInfo????

		common.PrintBridgeStatusTable(nodeInfo, ready, balance, localHead, networkHead, p2pInfo, bandwidthStats)
	},
}

func init() {
	rootCmd.AddCommand(statusCmd)
}
