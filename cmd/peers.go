/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"

	"github.com/packetstracer/celegram/common"
	"github.com/spf13/cobra"
)

var peersCmd = &cobra.Command{
	Use:   "peers",
	Short: "Get bridge peers data",
	Long:  `Get the bridge peers of the network.`,
	Run: func(cmd *cobra.Command, args []string) {
		common.PrintTitle("🪐 Bridge Peers started ", "h1")

		ctx := context.Background()
		url := common.GetNodeUrl()
		authToken := common.GetNodeAuthToken()

		peers, err := common.GetP2pPeers(ctx, url, authToken)
		if err != nil {
			common.PrintError("Could not get GetP2pPeers")
		}

		common.PrintPeersTable(peers)
	},
}

func init() {
	rootCmd.AddCommand(peersCmd)
}
