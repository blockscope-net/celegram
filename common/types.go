package common

type CatchupMetrics struct {
	ID      string
	ChainID string
	// Status        string
	Version       string
	Ready         bool
	Balance       uint64
	LocalHeight   uint64
	NetworkHeight uint64
	HeightSync    float64
	Bandwidth     struct {
		RateIn   float64
		RateOut  float64
		TotalIn  int64
		TotalOut int64
	}
}

// type LatestAlerts struct {
// 	Version             string
// 	Ready               bool
// 	BalanceThreshold    uint8
// 	HeightSyncThreshold uint8
// 	HeightHalt          uint64
// 	HeightHaltCounter   uint8
// 	BandwidthInHalt     uint64
// 	BandwidthOutHalt    uint64
// }
