package common

import (
	"context"
	"fmt"
	"strconv"

	"github.com/spf13/viper"
)

func SendStartAlert(ctx context.Context, url string, token string) {
	var discordMsg string
	var telegramMsg string
	var pagerDutyMsg string
	var slackMsg string

	if !isDiscordConfigured() {
		discordMsg = " not"
	}
	if !isTelegramConfigured() {
		telegramMsg = " not"
	}
	if !isPagerDutyConfigured() {
		pagerDutyMsg = " not"
	}
	if !isSlackConfigured() {
		slackMsg = " not"
	}

	discordMsg = "Discord:" + discordMsg + " enabled"
	telegramMsg = "Telegram:" + telegramMsg + " enabled"
	pagerDutyMsg = "PagerDuty:" + pagerDutyMsg + " enabled"
	slackMsg = "Slack:" + slackMsg + " enabled"

	p2pInfo, err := GetP2pInfo(ctx, url, token)
	if err != nil {
		PrintError("🆘 Error connecting to bridge RPC - url: " + url + " - token: " + token + " - error: " + err.Error())
	}

	Send("🪐 Celegram by LFV started ```Bridge ID: " + p2pInfo.ID.String() + "\n" +
		"Bridge RPC: " + url + "\n" +
		discordMsg + "\n" +
		telegramMsg + "\n" +
		pagerDutyMsg + "\n" +
		slackMsg + "```")
}

func ManageBridgeAlerts(metrics CatchupMetrics) {
	SaveMetrics(metrics)
	sendBridgeAlerts()
}

func sendBridgeAlerts() {
	if len(metricsStore) < TEMP_METRICS_LENGTH {
		return
	}

	previous := metricsStore[len(metricsStore)-2]
	current := metricsStore[len(metricsStore)-1]

	if current.ID == "" || previous.ID == "" {
		return
	}

	msg := getAlertMsg(&current, &previous)

	if len(msg) > 0 {
		msg = "**📢 Bridge** " + viper.GetString("") + "```" + msg + "```"
		Send(msg)
	}
}

func getAlertMsg(currentMetrics *CatchupMetrics, previousMetrics *CatchupMetrics) string {
	var msg string = ""
	var emoji, changeEmoji, label string

	if currentMetrics.Version != previousMetrics.Version &&
		len(currentMetrics.Version) > 0 && len(previousMetrics.Version) > 0 &&
		viper.GetBool("monitor_alerts_version") {
		msg += fmt.Sprintf("📦 API version upgrade: %s -> %s\n", previousMetrics.Version, currentMetrics.Version)
	}

	if currentMetrics.Ready != previousMetrics.Ready && viper.GetBool("monitor_alerts_ready") {
		if currentMetrics.Ready {
			msg += "🟢 Is ready again\n"
		} else {
			msg += "🔴 Is not ready\n"
		}
	}

	previousLevel := GetSyncThresholdLevel(previousMetrics.HeightSync)
	currentLevel := GetSyncThresholdLevel(currentMetrics.HeightSync)

	if currentLevel != previousLevel && viper.GetBool("monitor_alerts_height_halt") {
		emoji = GetSyncThresholdEmoji(float64(currentLevel))
		changeEmoji = GetChangeEmoji(currentMetrics.HeightSync, previousMetrics.HeightSync)
		label = FloatToStringTwoDecimals(currentMetrics.HeightSync) + " " + changeEmoji

		if currentLevel > previousLevel {
			msg += emoji + " is recovering height, synced " + label + " \n"
		} else {
			msg += emoji + " is missing height, synced" + label + " \n"
		}
	}

	previousLevel = GetBalanceThresholdLevel(previousMetrics.Balance)
	currentLevel = GetBalanceThresholdLevel(currentMetrics.Balance)

	if currentLevel != previousLevel && viper.GetBool("monitor_alerts_balance") {
		emoji = GetBalanceThresholdEmoji(currentLevel)
		changeEmoji = GetChangeEmoji(float64(currentMetrics.Balance), float64(previousMetrics.Balance))
		label = strconv.FormatUint(currentMetrics.Balance, 10) + " utia " + changeEmoji

		if currentLevel > previousLevel {
			msg += emoji + " balance is increasing " + label + " \n"
		} else {
			msg += emoji + " balance is decreasing " + label + " \n"
		}
	}

	return msg
}

func SendPingMsg(metrics CatchupMetrics) error {
	return Send(GetPingMsg(metrics))
}

func GetPingMsg(metrics CatchupMetrics) string {
	var msg string = ""
	var emoji, label string

	msg += "🔵 Version : " + metrics.Version + "\n"

	emoji = GetStatusEmoji(nil, metrics)
	label = GetStatusLabel(nil, metrics)
	msg += emoji + " Status : " + label + "\n"

	emoji = GetSyncThresholdEmoji(metrics.HeightSync)
	msg += emoji + " Height Sync : " + FloatToStringTwoDecimals(metrics.HeightSync) + "% (" +
		strconv.FormatUint(metrics.LocalHeight, 10) + " / " + strconv.FormatUint(metrics.NetworkHeight, 10) +
		")\n"

	emoji = GetBalanceThresholdEmoji(metrics.Balance)
	msg += emoji + " Balance : " + strconv.FormatUint(metrics.Balance, 10) + " utia\n"

	return "⏱️ Ping" + "```" + msg + "```"
}
