package common

import (
	"context"

	client "github.com/celestiaorg/celestia-openrpc"
	"github.com/celestiaorg/celestia-openrpc/types/header"
	"github.com/celestiaorg/celestia-openrpc/types/node"
	"github.com/celestiaorg/celestia-openrpc/types/sdk"
	"github.com/celestiaorg/go-header/sync"
	"github.com/libp2p/go-libp2p/core/metrics"
	"github.com/libp2p/go-libp2p/core/peer"
	"github.com/spf13/viper"
)

func GetNodeUrl() string {
	return viper.GetString("node_protocol") + "://" + viper.GetString("node_ip") + ":" + viper.GetString("node_port")
}

func GetNodeAuthToken() string {
	return viper.GetString("node_auth_token")
}

func GetNodeInfo(ctx context.Context, url string, token string) (node.Info, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return node.Info{}, err
	}

	nodeInfo, err := client.Node.Info(ctx)
	if err != nil {
		return node.Info{}, err
	}

	return nodeInfo, nil
}

func IsNodeReady(ctx context.Context, url string, token string) (bool, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return false, err
	}

	ready, err := client.Node.Ready(ctx)
	if err != nil {
		return false, err
	}

	return ready, nil
}

func GetHeaderSyncState(ctx context.Context, url string, token string) (sync.State, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return sync.State{}, err
	}

	syncState, err := client.Header.SyncState(ctx)
	if err != nil {
		return sync.State{}, err
	}

	return syncState, nil
}

func GetHeaderNetworkHead(ctx context.Context, url string, token string) (*header.ExtendedHeader, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return &header.ExtendedHeader{}, err
	}

	head, err := client.Header.NetworkHead(ctx)
	if err != nil {
		return &header.ExtendedHeader{}, err
	}

	return head, nil
}

func GetHeaderLocalHead(ctx context.Context, url string, token string) (*header.ExtendedHeader, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return &header.ExtendedHeader{}, err
	}

	head, err := client.Header.LocalHead(ctx)
	if err != nil {
		return &header.ExtendedHeader{}, err
	}

	return head, nil
}

func GetStateBalance(ctx context.Context, url string, token string) (*sdk.Coin, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return &sdk.Coin{}, err
	}

	balance, err := client.State.Balance(ctx)
	if err != nil {
		return &sdk.Coin{}, err
	}

	return balance, nil
}

func GetStateAccountAddress(ctx context.Context, url string, token string) (sdk.Address, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return nil, err
	}

	address, err := client.State.AccountAddress(ctx)
	if err != nil {
		return nil, err
	}

	return address, nil
}

func GetP2pInfo(ctx context.Context, url string, token string) (peer.AddrInfo, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return peer.AddrInfo{}, err
	}

	info, err := client.P2P.Info(ctx)
	if err != nil {
		return peer.AddrInfo{}, err
	}

	return info, nil
}

func GetP2pBandwidthStats(ctx context.Context, url string, token string) (metrics.Stats, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return metrics.Stats{}, err
	}

	stats, err := client.P2P.BandwidthStats(ctx)
	if err != nil {
		return metrics.Stats{}, err
	}

	return stats, nil
}

func GetP2pPeerInfo(ctx context.Context, url string, token string) (peer.AddrInfo, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return peer.AddrInfo{}, err
	}

	nodeInfo, err := GetP2pInfo(ctx, url, token)
	if err != nil {
		return peer.AddrInfo{}, err
	}

	peerInfo, err := client.P2P.PeerInfo(ctx, nodeInfo.ID)
	if err != nil {
		return peer.AddrInfo{}, err
	}

	return peerInfo, nil
}

func GetP2pPeers(ctx context.Context, url string, token string) ([]peer.ID, error) {
	client, err := client.NewClient(ctx, url, token)
	if err != nil {
		return []peer.ID{}, err
	}

	peers, err := client.P2P.Peers(ctx)
	if err != nil {
		return []peer.ID{}, err
	}

	return peers, nil
}
