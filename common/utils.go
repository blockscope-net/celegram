package common

import (
	"fmt"
	"math"
	"strings"
)

type CustomError struct {
	Msg string
}

func (e *CustomError) Error() string {
	return e.Msg
}

func FloatToStringTwoDecimals(number float64) string {
	return fmt.Sprint(math.Floor(number*100) / 100)
}

func ObfuscateAddress(address string, length int) string {
	var startOffset int

	if len(address) <= length {
		startOffset = len(address)
	} else {
		startOffset = len(address) - length
	}

	return "..." + address[startOffset:]
}

func ConvertStringLength(text string, length int, fillStart bool) string {
	if length <= len(text) {
		return text[:length]
	}

	fill := strings.Repeat(" ", length-len(text))

	if fillStart {
		return fill + text
	}

	return text + fill
}

func TruncateText(text string, min int, max int, splitChar string) string {
	if max > len(text) {
		return text
	}

	if splitChar == "" {
		splitChar = " "
	}

	return text[min:strings.LastIndex(text[:max], splitChar)]
}

func TruncateFloat(num float64) float64 {
	return math.Trunc(num*100) / 100
}

func RemoveInvertedQuotes(msg string) string {
	msg = strings.Replace(msg, "```", "\n\n", -1)
	msg = strings.Replace(msg, "`", "\n", -1)

	return msg
}

func GetBoolMapKeys(a map[string]bool) []string {
	keys := []string{}

	for key := range a {
		keys = append(keys, key)
	}

	return keys
}

func GetStringMap(keys []string) map[string]bool {
	result := make(map[string]bool)

	for _, key := range keys {
		result[key] = true
	}

	return result
}

func CalculateSyncUnitsPerSecond(previous uint64, current uint64, timeInterval uint64) float64 {
	if timeInterval == 0 {
		timeInterval = 10
	}

	var delta int64 = int64(current) - int64(previous)

	if delta < 0 || previous == 0 || current == 0 {
		return 0
	}

	var ups float64 = float64(delta) / float64(timeInterval)

	return math.Round(ups*10) / 10
}

func GetStatusLevel(err error, metrics CatchupMetrics) uint64 {
	if err != nil {
		return 4
	} else if metrics.Version == "" || metrics.LocalHeight == 0 {
		return 1
	} else if !metrics.Ready {
		return 3
	} else if metrics.HeightSync < NODE_SYNC_THRESHOLD_1 {
		return 2
	} else {
		return 0
	}
}

func GetStatusEmoji(err error, metrics CatchupMetrics) string {
	return []string{"🟢", "🟡", "🟠", "🟠", "🔴", "⚫️"}[GetStatusLevel(err, metrics)]
}

func GetStatusLabel(err error, metrics CatchupMetrics) string {
	errorLabel := "no status"
	if err != nil {
		errorLabel = err.Error()
	}

	levelLabel := []string{"OK", "Starting...", "Syncing...", "Not Ready", "Not Connected | Error: " + errorLabel, "unkown"}

	return levelLabel[GetStatusLevel(err, metrics)]
}

func GetSyncThresholdLevel(percentage float64) uint64 {
	switch {
	case percentage < NODE_SYNC_THRESHOLD_3:
		return 3
	case percentage < NODE_SYNC_THRESHOLD_2:
		return 2
	case percentage < NODE_SYNC_THRESHOLD_1:
		return 1
	case percentage >= NODE_SYNC_THRESHOLD_1:
		return 0
	default:
		return 4
	}
}

func GetSyncThresholdEmoji(percentage float64) string {
	return []string{"🟢", "🟡", "🟠", "🔴", "⚫️"}[GetSyncThresholdLevel(percentage)]
}

func GetBalanceThresholdLevel(balance uint64) uint64 {
	switch {
	case balance < WALLET_BALANCE_THRESHOLD_3:
		return 3
	case balance < WALLET_BALANCE_THRESHOLD_2:
		return 2
	case balance < WALLET_BALANCE_THRESHOLD_1:
		return 1
	case balance >= WALLET_BALANCE_THRESHOLD_1:
		return 0
	default:
		return 4
	}
}

func GetBalanceThresholdEmoji(balance uint64) string {
	return []string{"🟢", "🟡", "🟠", "🔴", "⚫️"}[GetBalanceThresholdLevel(balance)]
}

func GetChangeEmoji(a float64, b float64) string {
	if a > b {
		return "🔺"
	}
	if a < b {
		return "🔻"
	}

	return "🟰"
}
