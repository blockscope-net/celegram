package common

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"strings"

	"github.com/charmbracelet/lipgloss"
	"github.com/gtuk/discordwebhook"
	"github.com/slack-go/slack"
	"github.com/spf13/viper"
)

type PagerDutyPayload struct {
	Summary  string `json:"summary"`
	Severity string `json:"severity"`
	Source   string `json:"source"`
}

type PagerDutyEventRequest struct {
	Payload     PagerDutyPayload `json:"payload"`
	RoutingKey  string           `json:"routing_key"`
	EventAction string           `json:"event_action"`
}

var (
	Styles struct {
		Default           lipgloss.Style
		Success           lipgloss.Style
		Error             lipgloss.Style
		Warning           lipgloss.Style
		Title             lipgloss.Style
		TitlePrefix       lipgloss.Style
		TitleSuffix       lipgloss.Style
		Subtitle          lipgloss.Style
		SubtitlePrefix    lipgloss.Style
		SubtiitleSuffix   lipgloss.Style
		Sub3title         lipgloss.Style
		Sub3titlePrefix   lipgloss.Style
		Sub3tiitleSuffix  lipgloss.Style
		TableBorderStyle  lipgloss.Style
		TableHeaderStyle  lipgloss.Style
		TableEvenRowStyle lipgloss.Style
		TableOddRowStyle  lipgloss.Style
	}
)

func DeclareStyles() {
	Styles.Default = lipgloss.NewStyle().
		PaddingLeft(1).
		Foreground(lipgloss.Color("#e1bbfa"))

	Styles.Success = lipgloss.NewStyle().
		PaddingLeft(1).
		Foreground(lipgloss.Color("#79ed90"))

	Styles.Error = lipgloss.NewStyle().
		PaddingLeft(1).
		Foreground(lipgloss.Color("#ed2f59"))

	Styles.Warning = lipgloss.NewStyle().
		PaddingLeft(1).
		Foreground(lipgloss.Color("#ff9019"))

	Styles.Title = lipgloss.NewStyle().
		Bold(true).
		PaddingLeft(2).
		PaddingRight(2).
		Foreground(lipgloss.Color("#e99ffc"))

	Styles.TitlePrefix = lipgloss.NewStyle().
		Background(lipgloss.Color("#b931eb")).
		PaddingLeft(8)

	Styles.TitleSuffix = lipgloss.NewStyle().
		Background(lipgloss.Color("#b931eb")).
		PaddingRight(4)

	Styles.Subtitle = lipgloss.NewStyle().
		PaddingLeft(2).
		PaddingRight(2).
		Foreground(lipgloss.Color("#e2a7fa"))

	Styles.SubtitlePrefix = lipgloss.NewStyle().
		Background(lipgloss.Color("#d971f0")).
		PaddingLeft(4)

	Styles.SubtiitleSuffix = lipgloss.NewStyle().
		Background(lipgloss.Color("#d971f0")).
		PaddingRight(2)

	Styles.Sub3title = lipgloss.NewStyle().
		PaddingLeft(2).
		PaddingRight(2).
		Foreground(lipgloss.Color("#e2a7fa"))

	Styles.Sub3titlePrefix = lipgloss.NewStyle().
		Background(lipgloss.Color("#d984f0")).
		PaddingLeft(2)

	Styles.Sub3tiitleSuffix = lipgloss.NewStyle().
		Background(lipgloss.Color("#d984f0")).
		PaddingRight(0)

	Styles.TableBorderStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#ce28fc"))
	Styles.TableHeaderStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#a400c5")).Padding(0, 1).Bold(true)
	Styles.TableEvenRowStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#eb9ffc")).Padding(0, 1)
	Styles.TableOddRowStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#664173")).Padding(0, 1)
}

func PrintTitle(msg string, variant string) {
	switch variant {
	case "h3":
		fmt.Print(Styles.Sub3titlePrefix.Render(""))
		fmt.Println(Styles.Sub3title.Render(msg))

	case "h2":
		fmt.Print(Styles.SubtitlePrefix.Render(""))
		fmt.Println(Styles.Subtitle.Render(msg))

	default:
		fmt.Println("")
		fmt.Print(Styles.TitlePrefix.Render(""))
		fmt.Println(Styles.Title.Render(msg))
	}
}

func PrintError(msg string) {
	fmt.Println(Styles.Error.Render(msg))
}

func Send(msg string) error {
	// fmt.Println(time.Now().Format(time.UnixDate) + " : " + RemoveInvertedQuotes(msg))
	// fmt.Println(RemoveInvertedQuotes(msg))
	var err error

	if isDiscordConfigured() {
		err = SendDiscordWebhookMessage(msg)
	}

	if isTelegramConfigured() {
		err = SendTelegramMessage(msg)
	}

	if isPagerDutyConfigured() {
		err = SendPagerDutyMessage(msg, "alert")
	}

	if isSlackConfigured() {
		err = SendMessageToSlackChannel(msg, viper.GetString("slack_oauth_token"), viper.GetString("slack_channel_id"))
	}

	return err
}

func isDiscordConfigured() bool {
	return len(viper.GetString("discord_username")) > 0 && len(viper.GetString("discord_url")) > 0
}

func isTelegramConfigured() bool {
	return len(viper.GetString("telegram_bot_id")) > 0 && len(viper.GetString("telegram_chat_id")) > 0
}

func isPagerDutyConfigured() bool {
	return len(viper.GetString("pagerduty_integration_key")) > 0
}

func isSlackConfigured() bool {
	return len(viper.GetString("slack_oauth_token")) > 0 && len(viper.GetString("slack_channel_id")) > 0
}

func HasCommunicationChannelsEnabled() bool {
	return isDiscordConfigured() || isTelegramConfigured() || isPagerDutyConfigured() || isSlackConfigured()
}

// https://github.com/gtuk/discordwebhook
func SendDiscordWebhookMessage(msg string) error {
	username := viper.GetString("discord_username")

	message := discordwebhook.Message{
		Username: &username,
		Content:  &msg,
	}

	if err := discordwebhook.SendMessage(viper.GetString("discord_url"), message); err != nil {
		fmt.Println("Unable to send message to Discord")
		return err
	}

	return nil
}

func SendTelegramMessage(msg string) error {
	url := "https://api.telegram.org/bot" + viper.GetString("telegram_bot_id") + "/sendMessage"

	msg = RemoveInvertedQuotes(msg)

	payload, err := json.Marshal(map[string]string{
		"chat_id": viper.GetString("telegram_chat_id"),
		"text":    msg,
	})

	if err != nil {
		return err
	}

	response, err := http.Post(url, "application/json", bytes.NewBuffer(payload))
	if err != nil {
		return err
	}

	defer func(body io.ReadCloser) {
		if err := body.Close(); err != nil {
			log.Println("failed to close response body")
		}
	}(response.Body)

	if response.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to send successful request. Status was %q", response.Status)
	}

	return nil
}

func SendPagerDutyMessage(msg string, eventType string) error {
	headers := map[string]string{"Content-Type": "application/json"}
	url := "https://events.pagerduty.com/v2/enqueue"

	if eventType == "change" {
		url = "https://events.pagerduty.com/v2/change/enqueue"
	}

	msg = strings.Replace(msg, "```", "\n\n", -1)
	msg = strings.Replace(msg, "`", "\n", -1)
	msg = strings.Replace(msg, "**", "", -1)

	msg = strings.Replace(msg, "from", "", -1)
	re := regexp.MustCompile("<@[0-9]+>")
	msg = re.ReplaceAllString(msg, "")

	payload := PagerDutyEventRequest{
		Payload: PagerDutyPayload{
			Summary:  msg,
			Severity: "info",
			Source:   "Alert source",
		},
		RoutingKey:  viper.GetString("pagerduty_integration_key"),
		EventAction: "trigger",
	}

	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payloadBytes))
	if err != nil {
		return err
	}

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusAccepted {
		return fmt.Errorf("failed to send alert: %s", resp.Status)
	}

	return nil
}

func createSlackClient(token string) (*slack.Client, error) {
	client := slack.New(token)
	_, err := client.AuthTest()

	if err != nil {
		return nil, err
	}

	return client, nil
}

func SendMessageToSlackChannel(msg, token, channelID string) error {
	client, err := createSlackClient(token)
	if err != nil {
		return err
	}

	msg = strings.Replace(msg, "**", "", -1)

	msg = strings.Replace(msg, "from", "", -1)
	re := regexp.MustCompile("<@[0-9]+>")
	msg = re.ReplaceAllString(msg, "")

	_, _, err = client.PostMessage(channelID, slack.MsgOptionText(msg, false))
	if err != nil {
		return err
	}

	return nil
}

func SendMultipartMsg(msg string, maxLength int, delimiter string) {
	previousMessageEndPos := 0

	for previousMessageEndPos < len(msg)-1 {
		currentMsgEndPos := previousMessageEndPos + maxLength

		if currentMsgEndPos > len(msg) {
			currentMsgEndPos = len(msg)
		}

		msgPart := TruncateText(msg, previousMessageEndPos, currentMsgEndPos, "\n")
		previousMessageEndPos = strings.LastIndex(msg[:currentMsgEndPos], "\n")

		Send(delimiter + msgPart + delimiter)
	}
}
