package common

import (
	"context"
)

var metricsStore []CatchupMetrics

func FetchMetrics(ctx context.Context, url string, token string) (CatchupMetrics, error) {
	var metrics CatchupMetrics

	nodeInfo, err := GetNodeInfo(ctx, url, token)
	if err != nil {
		// PrintError("Could not get GetNodeInfo")
		return CatchupMetrics{}, err
	}

	ready, err := IsNodeReady(ctx, url, token)
	if err != nil {
		// PrintError("Could not get IsNodeReady")
		return CatchupMetrics{}, err
	}

	balance, err := GetStateBalance(ctx, url, token)
	if err != nil {
		// PrintError("Could not get GetStateBalance")
		return CatchupMetrics{}, err
	}

	localHead, err := GetHeaderLocalHead(ctx, url, token)
	if err != nil {
		// PrintError("Could not get GetHeaderLocalHead")
		return CatchupMetrics{}, err
	}

	networkHead, err := GetHeaderNetworkHead(ctx, url, token)
	if err != nil {
		// PrintError("Could not get GetHeaderNetworkHead")
		return CatchupMetrics{}, err
	}

	p2pInfo, err := GetP2pInfo(ctx, url, token)
	if err != nil {
		// PrintError("Could not get GetP2pInfo")
		return CatchupMetrics{}, err
	}

	bandwidthStats, err := GetP2pBandwidthStats(ctx, url, token)
	if err != nil {
		// PrintError("Could not get GetBandwidthStats")
		return CatchupMetrics{}, err
	}

	metrics.ID = p2pInfo.ID.String()
	metrics.ChainID = localHead.ChainID()
	metrics.Version = nodeInfo.APIVersion
	metrics.Ready = ready
	metrics.Balance = balance.Amount.Uint64()
	metrics.LocalHeight = localHead.Height()
	metrics.NetworkHeight = networkHead.Height()
	metrics.HeightSync = TruncateFloat(float64(metrics.LocalHeight) / float64(metrics.NetworkHeight) * 100)
	metrics.Bandwidth.RateIn = bandwidthStats.RateIn
	metrics.Bandwidth.RateOut = bandwidthStats.RateOut
	metrics.Bandwidth.TotalIn = bandwidthStats.TotalIn
	metrics.Bandwidth.TotalOut = bandwidthStats.TotalOut

	return metrics, nil
}

func SaveMetrics(metrics CatchupMetrics) {
	metricsStore = append(metricsStore, metrics)

	if len(metricsStore) > TEMP_METRICS_LENGTH {
		metricsStore = metricsStore[1:]
	}

	if len(metricsStore) == 1 {
		return
	}
}
