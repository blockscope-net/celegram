package common

import (
	"fmt"
	"strconv"

	"github.com/celestiaorg/celestia-openrpc/types/header"
	"github.com/celestiaorg/celestia-openrpc/types/node"
	"github.com/celestiaorg/celestia-openrpc/types/sdk"
	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/lipgloss/table"
	"github.com/libp2p/go-libp2p/core/metrics"
	"github.com/libp2p/go-libp2p/core/peer"
)

func PrintTable(headers []string, rows [][]string) {
	t := table.New().
		Border(lipgloss.NormalBorder()).
		BorderStyle(Styles.TableBorderStyle).
		StyleFunc(func(row, col int) lipgloss.Style {
			switch {
			case row == 0:
				return Styles.TableHeaderStyle
			case row%2 == 0:
				return Styles.TableEvenRowStyle
			default:
				return Styles.TableOddRowStyle
			}
		}).
		Headers(headers...).
		Rows(rows...)

	fmt.Println("")
	fmt.Println(t)
	fmt.Println("")
}

func PrintBridgeStatusTable(
	nodeInfo node.Info,
	ready bool,
	// address sdk.Address,
	balance *sdk.Coin,
	localHeader *header.ExtendedHeader,
	networkHeader *header.ExtendedHeader,
	p2pInfo peer.AddrInfo,
	bandwithStats metrics.Stats,
) {
	var rows [][]string = [][]string{}

	rows = append(rows, []string{"Bridge ID", p2pInfo.ID.String()})
	rows = append(rows, []string{"Chain ID", localHeader.ChainID()})
	rows = append(rows, []string{"Ready", strconv.FormatBool(ready)})
	rows = append(rows, []string{"API Version", nodeInfo.APIVersion})
	rows = append(rows, []string{"Bandwith Rate In", FloatToStringTwoDecimals(bandwithStats.RateIn)})
	rows = append(rows, []string{"Bandwith Rate Out", FloatToStringTwoDecimals(bandwithStats.RateOut)})
	rows = append(rows, []string{"Bandwith Total In", strconv.FormatInt(bandwithStats.TotalIn, 10)})
	rows = append(rows, []string{"Bandwith Total Out", strconv.FormatInt(bandwithStats.TotalOut, 10)})
	// rows = append(rows, []string{"Wallet Address", address.String()})
	rows = append(rows, []string{"Wallet Balance", balance.Amount.String() + " " + balance.Denom})
	rows = append(rows, []string{"Local Height", strconv.FormatUint(localHeader.Height(), 10)})
	rows = append(rows, []string{"Network Height", strconv.FormatUint(networkHeader.Height(), 10)})
	rows = append(rows, []string{"Time", networkHeader.Time().String()})
	rows = append(rows, []string{"Block Proposer", networkHeader.ValidatorSet.Proposer.Address.String()})
	rows = append(rows, []string{"Validator Count", strconv.FormatUint(uint64(len(networkHeader.ValidatorSet.Validators)), 10)})

	PrintTable([]string{"Field", "Value"}, rows)
}

func PrintPeersTable(
	peers []peer.ID,
) {
	var rows [][]string = [][]string{}

	for i, peer := range peers {
		rows = append(rows, []string{strconv.FormatInt(int64(i), 10), peer.String()})
	}

	PrintTable([]string{"Nr", "Peer ID"}, rows)
}

func PrintCatchupStatus(metrics CatchupMetrics, err error) string {
	versionEmoji := "🔵"

	statusEmoji := GetStatusEmoji(err, metrics)
	statusLabel := GetStatusLabel(err, metrics)

	balanceEmoji := GetBalanceThresholdEmoji(metrics.Balance)

	syncEmoji := GetSyncThresholdEmoji(metrics.HeightSync)
	syncText := FloatToStringTwoDecimals(metrics.HeightSync) + "%" +
		" (" + strconv.FormatUint(metrics.LocalHeight, 10) + "/" + strconv.FormatUint(metrics.NetworkHeight, 10) + ")"

	bandwidthEmoji := "🔵"
	bandwidthText := "Total " + strconv.FormatUint(uint64(metrics.Bandwidth.TotalIn), 10) + ":" + strconv.FormatUint(uint64(metrics.Bandwidth.TotalOut), 10) + " - " +
		"Rate " + FloatToStringTwoDecimals(metrics.Bandwidth.RateIn) + ":" + FloatToStringTwoDecimals(metrics.Bandwidth.RateOut)

	return "  " +
		statusEmoji + " Status: " + statusLabel + " | " +
		versionEmoji + " API: " + metrics.Version + " | " +
		balanceEmoji + " Balance: " + strconv.FormatUint(metrics.Balance, 10) + " utia | " +
		syncEmoji + " Height Sync: " + syncText + " | " +
		bandwidthEmoji + " Bandwidth (in:out): " + bandwidthText
}
