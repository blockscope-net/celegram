/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import (
	"github.com/packetstracer/celegram/cmd"
	"github.com/packetstracer/celegram/common"
)

func main() {
	common.DeclareStyles()
	cmd.Execute()
}
