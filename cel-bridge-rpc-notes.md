##auth token or node store (admin) for auth

AUTH_TOKEN=$(celestia bridge auth read)

AUTH_TOKEN=$(celestia bridge auth admin)

# for non mainnet networks needs to be specified like: AUTH_TOKEN=$(celestia bridge auth admin --p2p.network mocha-4)

export NODE_STORE=$HOME/.celestia-bridge

PORT=26658
PORT=12158
PORT=12058

# RPC

## Fraud

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "fraud.Subscribe","params": ["badencodingv0.1"]}' \
     http://localhost:$PORT | jq

## Header

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "header.SyncState","params": []}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "header.SyncWait","params": []}' \
     http://localhost:$PORT | jq

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "header.NetworkHead","params": []}' \
     http://localhost:$PORT | jq

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "header.LocalHead","params": []}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "header.WaitForHeight","params": [2046622]}' \
     http://localhost:$PORT | jq

## Node

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "node.Info","params": []}' \
     http://localhost:$PORT | jq

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "node.AuthNew","params": [["admin"]]}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "node.AuthVerify","params": ["eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJhZG1pbiJdfQ.erHLUEHo2fj-15h32cpNi1xwC0lM2sWLohk7M9pERa4"]}' \
     http://localhost:$PORT | jq

## State

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "state.Balance","params": []}' \
     http://localhost:$PORT | jq

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "state.AccountAddress","params": []}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "state.BalanceForAddress","params": ["celestia1yykuys9kd9clq463ppe6zearzqxucnw9k4u92f"]}' \
     http://localhost:$PORT | jq

### method not found

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "state.IsStopped","params": []}' \
     http://localhost:$PORT | jq

## P2P

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.Info","params": []}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.BandwidthForProtocol","params": ["/celestia/celestia/ipfs/bitswap"]}' \
     http://localhost:$PORT | jq

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.BandwidthStats","params": []}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.NATStatus","params": []}' \
     http://localhost:$PORT | jq

### XXX

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.Peers","params": []}' \
     http://localhost:$PORT | jq

### X

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.ListBlockedPeers","params": []}' \
     http://localhost:$PORT | jq

### X

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.ResourceState","params": []}' \
     http://localhost:$PORT | jq

##this peer shows data but not mine (12D3KooWQ267dzgXwugirG3hUdxR7vUTcUUcc5mU4HCqYaRtvvDp)
curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.BandwidthForPeer","params": ["12D3KooWQ267dzgXwugirG3hUdxR7vUTcUUcc5mU4HCqYaRtvvDp"]}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.Connectedness","params": ["12D3KooWQ267dzgXwugirG3hUdxR7vUTcUUcc5mU4HCqYaRtvvDp"]}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.IsProtected","params": ["12D3KooWQ267dzgXwugirG3hUdxR7vUTcUUcc5mU4HCqYaRtvvDp", "foo"]}' \
     http://localhost:$PORT | jq

## my peer id 12D3KooWG3ymdywdVPBpUswgVNe13BmUxg5Nk47CnC3BDFNMjUsQ

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "p2p.PeerInfo","params": ["12D3KooWG3ymdywdVPBpUswgVNe13BmUxg5Nk47CnC3BDFNMjUsQ"]}' \
     http://localhost:$PORT | jq

## DAS (only for light nodes)

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "das.SamplingStats","params": []}' \
     http://localhost:$PORT | jq

curl -X POST \
 -H 'Content-Type: application/json' \
 -H "Authorization: Bearer $AUTH_TOKEN" \
     -d '{"id": 1, "jsonrpc": "2.0","method": "das.WaitCatchUp","params": []}' \
     http://localhost:$PORT | jq

# CLI

# https://docs.celestia.org/developers/node-tutorial

## Header

# block by height

celestia header get-by-height 119494 --node.store $NODE_STORE

## P2P

celestia p2p info --node.store $NODE_STORE

## Node

celestia node info --node.store $NODE_STORE

## State

# get your node account balance

celestia state balance --node.store $NODE_STORE

# get your node account address

celestia state account-address --node.store $NODE_STORE

# get address balance

celestia state balance-for-address celestia192afvm4jlqmn5zcs9n7ce5p5g0uh3dqu59q63s --node.store $NODE_STORE

# transfer tokens to celestia192afvm4jlqmn5zcs9n7ce5p5g0uh3dqu59q63s -> 100000 amount, 8000 gas fee, 80000 gas limit

celestia state transfer celestia192afvm4jlqmn5zcs9n7ce5p5g0uh3dqu59q63s 100000 8000 80000 --node.store $NODE_STORE

## Blob

#submit data to nameSpace Id: 0x626C6F636B73636F7065 (blockscope encoded in Hex)
celestia blob submit 0x626C6F636B73636F7065 'foo' --node.store $NODE_STORE --fee 10000

{
"result": {
"height": 119494,
"commitment": "su3Qbo5b/k+TSI72dYky9SLWV8MwI4VJ0nZy6BMo2uk="
}
}

celestia blob submit 0x626C6F636B73636F7065 'bar' --node.store $NODE_STORE --fee 10000
{
"result": {
"height":4 119553,
"commitment": "nwSNBskJ/dIoW4NUrnydPzBnTf3zlWpT2wDjCs2UxHI="
}
}

celestia blob get 119494 0x626C6F636B73636F7065 su3Qbo5b/k+TSI72dYky9SLWV8MwI4VJ0nZy6BMo2uk= --node.store $NODE_STORE

celestia blob get-all 119494 0x626C6F636B73636F7065 --node.store $NODE_STORE

celestia blob get-all 119553 0x626C6F636B73636F7065 --node.store $NODE_STORE

## Share

celestia share get-by-namespace "$(celestia header get-by-height 119494 --node.store $NODE_STORE | jq '.result.dah.row_roots[0]' -r)" 0x626C6F636B73636F7065 --node.store $NODE_STORE

dydxprotocold tx staking edit-validator --new-moniker=" Coronex" --chain-id=dydx-mainnet-1 --gas=auto --website="https://coronex.xyz" --details="Welcome to the pinnacle of crypto validation excellence. An harmonious joint venture forged by Bloclick, GPValidator, Blockscope and LowFeeValidation, uniting unparalleled expertise. United we thrive!" --security-contact="bonsfi@lowfeevalidation.com" --identity="54788E59DF2F2272" --from=dydx-val --fees 952000000000000adydx

# OVH cancel server, password

xFYVg90nJxRLaHAO

# Rollap dymension status

curl -X POST --data '{"jsonrpc":"2.0","method":"eth_newBlockFilter","params":[],"id":1}' -H "Content-Type: application/json" http://localhost:26657
